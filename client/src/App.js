import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './components/Main';
import NotFound from './components/NotFound';

class App extends Component {
    render() { // eslint-disable-line
        return (
            <div className='app'>
                    <BrowserRouter>
                        <Switch>
                            <Route path='/' component={Main}/>
                            <Route component={NotFound}/>
                        </Switch>
                    </BrowserRouter>
            </div>
        );
    }
}
export default App;
