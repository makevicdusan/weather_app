
/*eslint-disable */
import React from 'react';
import { Route } from 'react-router-dom'; // eslint-disable-line
import LoginPage from '../containers/LoginPage';
import WeatherPage from '../containers/WeatherPage';
import '../css/Main.css';

export default class Main extends React.Component {
    constructor (props) {
        super(props);
    }
    componentDidMount () {
        this.props.history.push('/login');
    }
    render() { // eslint-disable-line
        return (
            <div className='main-wrap'>
                <Route path='/login' component={LoginPage}/>
                <Route path='/weather' component={WeatherPage}/>
            </div>
        );
    }
}
