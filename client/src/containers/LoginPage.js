/*eslint-disable */
import React from 'react';
import '../css/login.css';
import axiosInstance from '../axios_config';


export default class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            email: 'peraperic@gmail.com',
            password: 'password123'
        };
    }

    login () {
        axiosInstance({
            url: '/login',
            method: 'post',
            data: {
                email: this.state.email,
                password: this.state.password
            }
        }).then(response => {
            const { data } = response;
            localStorage.setItem('token', data.token);
            this.props.history.push('/weather');
        }).catch(err => {
            this.setState({ message: 'Wrong credentials!'});
        });
    }

    handleChange (event) {
        this.setState({ email: event.target.value });
    }

    handlePasswordChange (event) {
        this.setState({ password: event.target.value });
    }

    checkValid (value) {
        if(!value || typeof value !== 'string') return false;
        if(value.length < 6 || value.length > 100) return false;
        return true;
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleClick () {
        const isEmailValid = this.validateEmail(this.state.email);

        if(!isEmailValid) {
            this.setState({ message: 'Email not valid!'});
            return;
        }
        const isValid = this.checkValid(this.state.password);
        if(!isValid) {
            this.setState({ message: 'Your password must have minimum six simbols!'});
        } else {
            this.setState({ message: ''});
            this.login();
        }
    }

    render() {
        return (
            <div className="login-page">
                <div className="form">

                    <input type="text" 
                        value={this.state.email}
                        onChange={(event) => {
                            this.handleChange(event);}}/>

                    <input type="password"
                        value={this.state.password}
                        onChange={(event) => {
                            this.handlePasswordChange(event);}}/>
                    <button onClick={() => {
                        this.handleClick();
                    }}>login</button>
                    <p className="message">{this.state.message}</p>
                </div>
            </div>
        );
    }
}
