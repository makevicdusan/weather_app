/*eslint-disable */
import React from 'react';
import '../css/weather.css';
import axios from 'axios';
import axiosInstance from '../axios_config';

export default class WeatherPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAuthorized: false,
            image: 'assets/images/Partly_Cloudy-128.png'
        };
        this.celsius = 'assets/images/celsius.png';
    }

    componentDidMount () {
        const token = localStorage.getItem('token');
        axiosInstance.get('/weather', {
            headers: { 'Authorization' : `Bearer ${token}`}
        }).then((response) => {
            this.setState({isAuthorized: true});
            this.addData(response.data);
        }).catch(err => {
            this.props.history.push('/login');
        });
    }

    kelvinToCelsius (kelvin) {
        const celsius = Math.round(kelvin - 273.15);
        return parseInt(celsius);
    }

    convertTime (timestamp) {
        const date = new Date(timestamp * 1000);
        const hours = date.getHours();
        const minutes = "0" + date.getMinutes();
        const seconds = "0" + date.getSeconds();
        const d = date.getDate();
        const month = date.getMonth();
        const year = date.getFullYear();
        const formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
        const formatedDate = `${d}/${month}/${year}`;
        return `${formatedDate} ${formattedTime}`;
    }

    renderImage() {
        if(!this.state.image) {
            return '';
        }
        return (
            <div className='image-wrapper'><img src={this.state.image}></img></div>
        );
    }

    renderCelsiusSymbol () {
        return <label>{'\u00b0'}C</label>;
    }

    render() {
        
        if(!this.state.isAuthorized) {
            return (
                <div>Checking...</div>
            );
        }
        return (
            <div className='weather-page'>
                <div className='weather-container'>
                    <div className='left-container'>
                        <div>
                            {this.renderImage()}
                        </div>
                    </div>
                    <div className='right-container'>
                        <div>
                            <label>Temperature: {this.state.temperature} </label> {this.renderCelsiusSymbol()}
                        </div>
                        <div>
                            <label>Pressure: {this.state.pressure} mb</label>
                        </div>
                        <div>
                            <label>Humidity: {this.state.humidity}%</label>
                        </div>
                        <div>
                            <label>Wind: {this.state.wind} m/s</label>
                        </div>
                        <div>
                            <label>City: {this.state.cityName}</label>
                        </div>
                        <div>
                            <label>{this.state.time}</label>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    addData (data) {
        if(!data) return;
        const weather = data.weather[0];

        const temperature = this.kelvinToCelsius(data.main.temp);
        // const feelsLikeTemp = this.kelvinToCelsius(data.main.feels_like);
        const pressure = data.main.pressure;
        const humidity = data.main.humidity;
        const wind = data.wind.speed;
        const cityName = data.name;
        const time = this.convertTime(data.dt);


        const weatherType = weather.main.toLowerCase();
        if(weatherType === 'cloudy') {
            this.setState({ image: 'assets/images/Partly_Cloudy-128.png'});
        } else if(weatherType === 'rain') {
            this.setState({ image: 'assets/images/Light_Rain-128.png'});
        } else if(weatherType === 'snow') {
            this.setState({ image: 'assets/images/Snow-128.png'});
        } else if(weatherType === 'sunny') {
            this.setState({ image: 'assets/images/Sunny-128.png'});
        } else if(weatherType === 'clouds') {
            this.setState({ image: 'assets/images/Overcast-128.png'});
        }
        this.setState({temperature});
        this.setState({pressure});
        this.setState({humidity});
        this.setState({wind});
        this.setState({cityName});
        this.setState({time});
    }
 
}

/*eslint-disable */