#**Game in react**

This is a test used for the assessment of the javascript developer position.

#**Download**

```
cd /path_to_file_where_you_want_to_clone
```
```
git clone https://makevicdusan@bitbucket.org/makevicdusan/weather_app.git
```

#**Installation**

```
cd weather_app/server
```
```
npm install
```

#**Run application**

```
npm run dev
```

type in your browser
```
localhost:3000 
```
