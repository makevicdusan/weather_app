
const loginController = require('./login');
const weatherController = require('./weather');
const checkController = require('./check');

const routes = (app) => {
    app.post('/login', loginController.login);
    app.get('/weather', 
    checkController.check,
    weatherController.getWeather);
};

module.exports = routes;