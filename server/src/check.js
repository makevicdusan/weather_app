const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = {
    check (req, res, next) {
        const bearer = req.get('Authorization');

        if(!bearer) {
            res.status(401).json({ error: 'You are not authorized!' });
            return;
        }

        const token = bearer.split(' ')[1];

        fs.readFile('public.key.pub', function (err, key) {
            if(err) {
                res.status(500).json({error: 'Server error'});
                return;
            }
            jwt.verify(token, key, function(err, decoded) {
                // console.log('decoded - ', decoded);
                if(err) {
                    res.status(401).json({ error: 'You are not authorized!' });
                } else {
                    res.user = decoded;
                    next();
                }
            });
        });  
    }
}