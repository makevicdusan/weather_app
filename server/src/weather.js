const config = require('../config');
const axios = require('axios');

module.exports = {
    async getWeather(req, res) {
        try {
            const link = config.weather_api.link;
            const apiKey = config.weather_api.api_key;
            const cityId = config.weather_api.city_id;

          const result = await axios.get(`${link}`, {
                params: {
                    id: cityId,
                    appid: apiKey
                }
            });

            if(result.status == 200) {
                res.status(200).json(result.data);
            }

        } catch(err) {
            res.status(204);
        }
        
    }


};