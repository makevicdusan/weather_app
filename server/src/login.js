const crypto = require('crypto');
const data = require('../data');
const fs = require('fs');
const jwt = require('jsonwebtoken');


module.exports = {
    login(req, res) {
        try {
            const password = req.body.password;
            const email = req.body.email;
            
            const pass = crypto.createHash("sha256")
            .update(`${password}`)
            .digest("hex");
            
            // authentication
            let user;
            data.users.forEach(u => {
                if(pass === u.password && email === u.email) {
                    user = u; 
                }
            });
        
            if(!user) {
                res.status(401).json({error: 'Wrong credentials!'});
            } else {
                fs.readFile('private.key', function (err, key) {
                    jwt.sign({ 
                        user, iat: Math.floor(Date.now() / 1000) + (60 * 60 * 20) 
                    }, key, { algorithm: 'RS256' }, function(err, token) {
                        if(!err) {
                            res.status(200).json({ token });
                        } else {
                            res.status(401).json({ error: 'Wrong credentials!'});
                        }
                        
                     });
                });
            }
        } catch(err) {
            res.status(401).json({ error: 'Wrong credentials!'});
        }
    }
};