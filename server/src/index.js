const express = require('express');
const app = express();
const routes = require('./routes');


app.use(express.json());
app.use(express.static('public'));



routes(app);



app.listen(3000);